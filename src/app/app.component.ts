import { Component,ViewChild } from '@angular/core';
import { SidenavService } from './shared/sidenav.service';
import { MatDrawer } from '@angular/material/sidenav';
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  expandSideNav: boolean = false;
  showHeader : boolean =false;

  userDetails:any=[];
  selectedItem: any;
  loggedinuser: any;

  sidenavElements: object[] = [
    { id: 1, name: 'Dashboard', routerLink: "/dashboard", icon: "dashboard" },
    { id: 2, name: 'Users', routerLink: "/usermanagement", icon: "people" },
    { id: 3, name: 'configure Paths', routerLink: "/configurepaths", icon: "add_location" }
  ];

  @ViewChild('drawer') public sidenav: MatDrawer;

constructor(public sidenavservice: SidenavService){
  this.sidenavservice.showSideNav.subscribe(val => {
    this.showHeader = val;
    if (val) {
      this.sidenav.open();
    }
  });
}
  ngOnInit() { 
    // this.loggedinuser = localStorage.getItem("username"); 
    if (localStorage.getItem("railways_token")) {
      this.sidenav.open();
    } else {
      this.sidenav.close();
    }
   
  }

  increase(){
    this.expandSideNav = true;
  }
  decrease(){
    this.expandSideNav = false;
  }

}
